package com.example.projectoandroid3

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.myapplication.ui.theme.MyApplicationTheme
import com.example.myapplication.Primera
import com.google.firebase.auth.FirebaseAuth
import javax.security.auth.login.LoginException

class MainActivity : ComponentActivity() {

    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {

        firebaseAuth = FirebaseAuth.getInstance()

        super.onCreate(savedInstanceState)

        setContent {
            MyApplicationTheme() {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    LoginView()
                }
            }
        }
    }

        fun login(email: String, password: String) {
            // Verificar que no están vacías
            if (email.isNotEmpty() && password.isNotEmpty()) {
                firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Iniciar sesión exitosa, enviar al usuario a la pantalla de inicio
                            //se ha creado una clase nueva
                            val intent = Intent(this, Primera::class.java)
                            startActivity(intent)
                        } else {
                            // Mostrar un mensaje de error si la autenticación falla
                            Toast.makeText(this, "Email o contraseña inválidos", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
            } else {
                // Mostrar un mensaje si el email o la contraseña están vacíos
                Toast.makeText(this, "Inserta un email y una contraseña", Toast.LENGTH_SHORT).show()
            }
        }


        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun LoginView() {
            var email by remember {
                mutableStateOf("")
            }
            var password by remember {
                mutableStateOf("")
            }
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "Email")
                TextField(

                    value = email,
                    onValueChange = { email = it },
                    //Placeholder
                    label = { Text(text = "Esto es el placeholder") },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Email
                    ),
                    modifier = Modifier.padding(10.dp)
                )

                Text(text = "Contraseña")
                TextField(

                    value = password,
                    onValueChange = { password = it },
                    //Placeholder
                    label = { Text(text = "Esto es el placeholder") },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Password,
                        imeAction = ImeAction.None
                    ),
                    modifier = Modifier.padding(10.dp)
                )
                Button(onClick = { login(email, password) }) {
                    Text(text = "Login")
                }
            }
        }


    @Preview(showBackground = true)
    @Composable
    fun RegistroPreview() {
        MyApplicationTheme {
            LoginView()
        }
    }
}