package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.myapplication.ui.theme.MyApplicationTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class RegistroActivity : ComponentActivity() {
    private lateinit var firebaseAuth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firebaseAuth = FirebaseAuth.getInstance()

        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    RegistroView()
                }
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun RegistroView() {
        var email by remember {
            mutableStateOf("")
        }
        var password by remember {
            mutableStateOf("")
        }
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = "Email")
            TextField(

                value = email,
                onValueChange = { email = it },
                //Placeholder
                label = { Text(text = "Esto es el placeholder") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email
                ),
                modifier = Modifier.padding(10.dp)
            )

            Text(text = "Contraseña")
            TextField(

                value = password,
                onValueChange = { password = it },
                //Placeholder
                label = { Text(text = "Esto es el placeholder") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password,
                    imeAction = ImeAction.None
                ),
                modifier = Modifier.padding(10.dp)
            )
            Button(onClick = { registro(email, password) }) {
                Text(text = "Registrar")
            }
        }
    }

    fun registro(email: String, pass: String) {
        // Verifica que el correo electrónico y la contraseña no estén vacíos
        if(email.isNotEmpty() && pass.isNotEmpty() ){
            // Si los campos no están vacíos, intenta crear un nuevo usuario en Firebase Authentication
            firebaseAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this){ task ->
                // Cuando se completa la tarea de crear un usuario
                val user: FirebaseUser = firebaseAuth.currentUser!! // Obtiene el usuario actualmente autenticado (sabemos que no es nulo)
                verifyEmail(user) // Llama a la función para enviar un correo electrónico de verificación
                // Si todo sale bien, muestra la actividad "Segunda"
                startActivity(Intent(this, Primera::class.java))
            }.addOnFailureListener {
                // Si falla la creación del usuario, muestra un mensaje de error
                Toast.makeText(this, "Error de autenticación", Toast.LENGTH_SHORT).show()
            }
        } else {
            // Si el correo electrónico o la contraseña están vacíos, muestra un mensaje de advertencia
            Toast.makeText(this, "Inserta los datos correctamente", Toast.LENGTH_SHORT).show()
        }
    }

    fun verifyEmail(user: FirebaseUser){
        // Envía un correo electrónico de verificación al usuario
        user.sendEmailVerification().addOnCompleteListener(this){ task ->
            // Cuando se completa la tarea de enviar el correo electrónico de verificación
            if(task.isSuccessful){
                // Si tiene éxito, muestra un mensaje indicando que el correo electrónico ha sido verificado
                Toast.makeText(this, "Email verificado", Toast.LENGTH_SHORT).show()
            } else {
                // Si falla, muestra un mensaje de error
                Toast.makeText(this, "Error al verificar email", Toast.LENGTH_SHORT).show()
            }
        }
    }


    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview2() {
        MyApplicationTheme {
            RegistroView()
        }
    }
}